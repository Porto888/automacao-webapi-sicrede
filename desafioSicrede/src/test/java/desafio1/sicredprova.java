package desafio1;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class sicredprova {
	@Test
	public void testeCadastroSicred() {

		System.setProperty("WebDriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");
		WebDriver navegador = new ChromeDriver();

		navegador.get("https://www.grocerycrud.com/v1.x/demo/bootstrap_theme");
		navegador.manage().window().maximize();
		navegador.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		WebElement selectElement = navegador.findElement(By.id("switch-version-select"));
		Select selectObject = new Select(selectElement);
		selectObject.selectByValue("/v1.x/demo/bootstrap_theme_v4");

		navegador.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();
		navegador.findElement(By.name("customerName")).sendKeys("Teste Sicred");
		navegador.findElement(By.name("contactLastName")).sendKeys("Teste");
		navegador.findElement(By.name("contactFirstName")).sendKeys("Guilherme Porto de Souza");
		navegador.findElement(By.name("phone")).sendKeys("11 98749-3716");
		navegador.findElement(By.name("addressLine1")).sendKeys("Av Assis Brasil, 3970");
		navegador.findElement(By.name("addressLine2")).sendKeys("Torre D");
		navegador.findElement(By.name("city")).sendKeys("Porto Alegre");
		navegador.findElement(By.name("state")).sendKeys("RS");
		navegador.findElement(By.name("postalCode")).sendKeys("91000-000");
		navegador.findElement(By.name("country")).sendKeys("Brasil");
		navegador.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/a/span")).click();
		navegador.findElement(By.cssSelector("#field_salesRepEmployeeNumber_chosen > div > div > input[type=text]"))
				.sendKeys("Fixter");
		navegador.findElement(By.cssSelector("#field_salesRepEmployeeNumber_chosen > div > div > input[type=text]"))
				.click();
		navegador.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input")).submit();
		navegador.findElement(By.name("creditLimit")).sendKeys("200");
		navegador.findElement(By.xpath("//*[@id=\"form-button-save\"]")).click();

		navegador.close();

	}

	{
	}

}
